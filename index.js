const env = require('dotenv');
env.config();

console.log("Hello everybody")
console.log(`Welcome to application : ${process.env.APPLICATION_NAME}`)
console.log(`Debug mode : ${process.env.APPLICATION_DEBUG_MODE}`)
const now = new Date();

if (process.env.APPLICATION_DEBUG_MODE === 'true') {
    console.log(`Debug mode active, so you can look time in this app ${now}`)
}