FROM keymetrics/pm2:10-alpine

RUN apk update && apk upgrade && \
  apk add --no-cache \
    bash \
    git \
    curl \
    openssh

MAINTAINER rifki m bogara

RUN mkdir -p /home/rifkibog/dockerfile/src/softdave
RUN pwd
WORKDIR /home/rifkibog/dockerfile/src/softdave
RUN pwd

ENV APPLICATION_NAME ${APPLICATION_NAME}
ENV APPLICATION_DEBUG_MODE ${APPLICATION_DEBUG_MODE}

RUN echo "APPLICATION_NAME=${APPLICATION_NAME}" >> /usr/src/softdev/.env
RUN echo "APPLICATION_DEBUG_MODE=${APPLICATION_DEBUG_MODE}" >> /usr/src/softdev/.env

COPY package*.json ./
RUN npm cache clean --force
RUN npm install
COPY . .

EXPOSE 3000

CMD [ "pm2-runtime", "start", "pm2.json", "--env", "production"]
